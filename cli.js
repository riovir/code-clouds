#!/usr/bin/env node

const program = require('commander');
const { version } = require('./package');
const codeCloud = require('./lib');
const { generateConfig, loadConfig } = require('./lib');

const description =
`Gathers words from a code base to be used by a word cloud app.`;

program
  .version(version)
  .usage('[options]')
  .description(description)
  .option('-i, --init', 'generate a default config in the current directory if none exists')
  .option('-c, --config [path]', 'path to config CommonJS file')
  .option('-s, --source [path]', 'path to the code base')
  .option('-t, --target [path]', 'path to generated word cloud data')
  .parse(process.argv);

runCodeCloud(program);

function runCodeCloud({ source, target, init, config }) {
  if (init) {
    return generateConfig();
  }

  const cloudConfig = loadConfig({ path: config });
  cloudConfig.source = source || cloudConfig.source;
  cloudConfig.target = target || cloudConfig.target;
  listRules(cloudConfig.rules);
  return codeCloud(cloudConfig, { source, target })
    .catch(err => { console.log(err); });
}

function listRules(rules) {
  rules.map(formatRule)
    .forEach(rule => { console.log(rule); });
}

function formatRule({ test, name = 'rule' }) {
  return `${test} - ${name}`;
}
