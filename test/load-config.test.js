console.log = jest.fn();

const path = require('path');
const { loadConfig } = require('../lib');

test('returns default config when not found', () => {
  expect(loadConfig(/* The project root does not have a default config */).target).toBe('code-cloud');
});

test('looks in cwd by default', () => {
  expect(loadConfig({ path: 'test/fixtures/custom.config.js' }).source).toBe('custom.config.source');
});

test('looks for cloud.config.js by default', () => {
  expect(load().source).toBe('cloud.config.source');
});

test('looks for specific config when declared', () => {
  expect(load({ path: 'custom.config.js' }).source).toBe('custom.config.source');
});

test('ignored root if path is absolute', () => {
  expect(load({
    root: 'ignored',
    path: path.resolve(__dirname, './fixtures/custom.config')
  }).source).toBe('custom.config.source');
});

test('throws error when config is invalid', () => {
  expect(() => load({ path: 'invalid.config.js' })).toThrow();
});

function load(args) {
  const root = path.resolve(__dirname, './fixtures');
  return loadConfig(Object.assign({ root }, args));
}
