module.exports = {
  source: 'cloud.config.source',
  target: 'cloud.config.target',
  include: ['cloud.config.test'],
  rules: [{ test: /\.vue$/ }]
};
