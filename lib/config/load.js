const path = require('path');
const defaults = require('./defaults');
const defaultRules = require('../rules');

module.exports = function generate({
  root = process.cwd(),
  path: _path = 'cloud.config.js'
} = {}) {
  try {
    const configPath = path.resolve(root, _path);
    const userConfig = require(configPath);
    const rules = defaultRules
      .concat(userConfig.rules);
    return Object.assign({}, defaults, userConfig, { rules });
  }
  catch (err) {
    if (err.code !== 'MODULE_NOT_FOUND') { throw err; }
    return defaults;
  }
};
