const generate = require('./generate');

test('copies default file to cwd', () => {
  const fs = { copyFileSync: jest.fn() };
  const copyCalls = fs.copyFileSync.mock.calls;
  generate({ fs });
  expect(copyCalls.length).toBe(1);
  expect(copyCalls[0][0]).toMatch(/defaults\.js$/);
  expect(copyCalls[0][1]).toMatch(/cloud\.config\.js$/);
});
