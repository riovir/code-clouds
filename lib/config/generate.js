const path = require('path');
const _fs = require('fs');

module.exports = function generate({ fs = _fs, target = process.cwd() } = {}) {
  const source = path.resolve(__dirname, 'defaults.js');
  const targetFile = path.resolve(target, 'cloud.config.js');
  fs.copyFileSync(source, targetFile);
};
