module.exports = {
  source: '.',
  target: 'code-cloud',
  include: [
    /^[^/]+\.(js|java|vue)$/,
    /^src\/.*\.(js|java|vue)$/,
    /^lib\/.*\.js$/,
    /^test\/.*\.(js|java)$/,
    /^server\/.*\.js$/,
  ],
  exclude: [/* Same format as include */],
  rules: [
    /*
    {
      name: 'disable default vue processing rule',
      test: /\.vue$/,
      convert: false // can be any falsy value
    },
    */
    /*
    {
      name: 'ignore class, get, and set in java',
      test: /\.java$/,
      convert: code => code.replace(/(class|get|set)/g, '')
    }
    */
  ]
};
