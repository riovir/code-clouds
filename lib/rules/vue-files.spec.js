const { pickScript } = require('./vue-files');

describe('pickScript', () => {
  test('picks content between <script> tags', () => {
    expect(pickScript('<script>content</script>')).toBe('content');
  });

  test('allows attributes in <script> tag', () => {
    expect(pickScript('<script lang="pug">content</script>')).toBe('content');
  });

  test('allows multi line <script>', () => {
    expect(pickScript('<script lang="pug">\n\tcontent\n</script>')).toBe('content');
  });

  test('allows multi line content', () => {
    expect(pickScript('<script lang="pug">\nhello\nscript\n</script>')).toBe('hello\nscript');
  });

  test('returns empty string when not <script> tag is present', () => {
    expect(pickScript('<template>no html</template><style>no css</style><extra>no extra</extra>')).toBe('');
  });
});
