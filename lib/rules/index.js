const vue = require('./vue-files');

module.exports = [
  { test: /\.vue$/, convert: vue.pickScript, name: 'only pick script from vue files' }
];
