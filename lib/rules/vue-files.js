const { defaultTo, find, pipe, prop, propEq, trim } = require('ramda');
const { parse } = require("xml-parse");

module.exports = { pickScript };

function pickScript(contents) {
  return pipe(
    parse,
    find(propEq('tagName', 'script')),
    defaultTo({ innerXML: '' }),
    prop('innerXML'),
    trim
  )(contents);
}
