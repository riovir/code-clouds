const { generate: generateConfig, load: loadConfig } = require('./config');
const cloudifyCode = require('./cloudify-code');

module.exports = cloudifyCode;
module.exports.loadConfig = loadConfig;
module.exports.generateConfig = generateConfig;
