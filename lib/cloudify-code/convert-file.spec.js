const { __, always } = require('ramda');
const convertFile = require('./convert-file');

const convert = (contents, path) => `converted ${contents} of ${path}`;
const process = convertFile(__, {
  name: 'file',
  relativePath: 'file.ext',
  read: async () => 'contents'
});

test('not matching any rule returns contents as is', async () => {
  expect(await process([])).toBe('file\ncontents\n');
});

test('adds file name first in its own line', async () => {
  expect(await process([])).toBe('file\ncontents\n');
});

test('only calls converter(s) of last matching rule', async () => {
  const rules = [
    { test: /\.ext$/, convert: always('irrelevant') },
    { test: /\.ext$/, convert }
  ];
  expect(await process(rules)).toBe('file\nconverted contents of file.ext\n');
});

test('calls converter with contents and relativePath', async () => {
  const rules = [{ test: /\.ext$/, convert }];
  expect(await process(rules)).toBe('file\nconverted contents of file.ext\n');
});

test('falsy converter returns content as is', async () => {
  const rules = [{ test: /\.ext$/ }];
  expect(await process(rules)).toBe('file\ncontents\n');
});

test('calls multiple converters from left to right', async () => {
  const first = contents => `first ${contents}`;
  const second = (contents, path) => `${contents} then ${path}`;
  const rules = [{ test: /\.ext$/, convert: [first, second] }];
  expect(await process(rules)).toBe('file\nfirst contents then file.ext\n');
});
