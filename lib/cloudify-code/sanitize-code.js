const { pipe, replace, toLower, trim } = require('ramda');
const { remove: removeAccents } = require('remove-accents');

module.exports = function sanitizeCode(text = '') {
  return pipe(
    removeAccents,
    replace(/[^a-zA-Z]/g, ' '),
    replace(/([A-Z])([a-z])/g, ' $1$2'),
    replace(/([a-z])([A-Z])/g, '$1 $2'),
    replace(/\s+/g, ' '),
    trim,
    toLower
  )(text);
};
