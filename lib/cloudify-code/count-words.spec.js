const countWords = require('./count-words');

test('returns empty object for undefined', () => {
  expect(countWords()).toEqual({});
});

test('ignores multiple spaces', () => {
  expect(countWords('hello     world')).toEqual({ hello: 1, world: 1 });
});

test('counts words', () => {
  const code = 'hello world hello HELLO';
  expect(countWords(code)).toEqual({ hello: 2, world: 1, HELLO: 1 });
});
