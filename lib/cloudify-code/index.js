const { bind, concat, map, pipe, pipeP, prop, reduce } = require('ramda');
const chewDir = require('chew-dir');

const sanitizeCode = require('./sanitize-code');
const convertFile = require('./convert-file');
const countWords = require('./count-words');

module.exports = function cloudifyCode({
  source = 'lib',
  target = 'code-cloud',
  include = '.js',
  exclude = [],
  rules = [],
  _chewDir = chewDir
} = {}) {
  const convert = convertFile(rules);

  async function processAll(files, { outputFile }) {
    const cloudify = pipeP(
      pipe(map(convert), bind(Promise.all, Promise)),
      reduce(concat, ''),
      sanitizeCode
    );
    const words = await cloudify(files);
    outputFile('cloud.txt', words);

    outputFile('source-files.txt', files
      .map(prop('relativePath'))
      .join('\n'));

    const cloudData = countWords(words);
    outputFile('cloud.json', JSON.stringify(cloudData, null, 2));
  }

  return _chewDir({ source, target, include, exclude, processAll, deep: true });
};
