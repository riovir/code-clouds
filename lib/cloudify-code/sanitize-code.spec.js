const sanitizeCode = require('./sanitize-code');

test('returns empty string for undefined', () => {
  expect(sanitizeCode()).toBe('');
});

test('ignores symbols', () => {
  expect(sanitizeCode(`"'\\/*~!@#$%^&*)_+}{:<>?@`)).toBe('');
});

test('ignores whitespace', () => {
  expect(sanitizeCode(' hello\thello\n')).toBe('hello hello');
});

test('ignores casing', () => {
  expect(sanitizeCode('hello Hello HELLO')).toBe('hello hello hello');
});

test('ignores accents', () => {
  expect(sanitizeCode('hello Hélló HÉLLÖ')).toBe('hello hello hello');
});

test('splits by CamelCase', () => {
  expect(sanitizeCode('CamelCase')).toBe('camel case');
});

test('does not confuae ALL CAPS as camel case', () => {
  expect(sanitizeCode('LOUD')).toBe('loud');
});

test('can split with CamelCase and ALL CAPS', () => {
  expect(sanitizeCode('yellALOUD')).toBe('yell aloud');
});

test('strips out code', () => {
  const code = `public void helloWorld(String hello) {
    System.err.println(hello + " world");
    new URI();
    int[] foo = {"red", "green", "blue"}
  }`;
  expect(sanitizeCode(code)).toBe(
    'public void hello world string hello ' +
    'system err println hello world ' +
    'new uri ' +
    'int foo red green blue'
  );
});
