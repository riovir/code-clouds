const { curry, defaultTo, findLast, identity, pipe, prop } = require('ramda');

module.exports = curry(processFile);

async function processFile(rules, { name, relativePath, read }) {
  const text = await read();
  const converter = findConverterFor({ rules, relativePath });
  const converters = converter.reduce ? converter : [converter];
  const content = converters.reduce((text, convert) => convert(text, relativePath), text);
  return `${name}\n${content}\n`;
}

function findConverterFor({ rules, relativePath }) {
  const matchesPath = ({ test }) => test.test(relativePath);
  return pipe(
    findLast(matchesPath),
    defaultTo({ convert: identity }),
    prop('convert'),
    defaultTo(identity)
  )(rules);
}
