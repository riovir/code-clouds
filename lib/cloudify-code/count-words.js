const { countBy, identity, isEmpty, pipe, reject, split } = require('ramda');

module.exports = function countWords(text = '') {
  return pipe(
    split(' '),
    reject(isEmpty),
    countBy(identity)
  )(text);
};
