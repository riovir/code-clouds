
const { replace } = require('ramda');
const cloudifyCode = require('./index');

describe('calls chewDir once', () => {
  const config = {
    source: 'test-source',
    target: 'test-target',
    include: 'test-include',
    exclude: 'test-exclude',
    other: 'not-passed-along',
  };
  let chewDirConfig;

  beforeEach(() => {
    const _chewDir = jest.fn();
    cloudifyCode(Object.assign({ _chewDir }, config));
    expect(_chewDir.mock.calls.length).toBe(1);
    chewDirConfig = _chewDir.mock.calls[0][0];
  });

  test('with source', () => expect(chewDirConfig.source).toBe('test-source'));
  test('with target', () => expect(chewDirConfig.target).toBe('test-target'));
  test('with include', () => expect(chewDirConfig.include).toBe('test-include'));
  test('with exclude', () => expect(chewDirConfig.exclude).toBe('test-exclude'));
  test('with deep scanning', () => expect(chewDirConfig.deep).toBe(true));
  test('without other props', () => expect(chewDirConfig.other).toBeUndefined());
});

describe('after processing files', () => {
  const fileOne = TestFile('code-one', 'hello => `${hello}, world!;');
  const fileTwo = TestFile('code-two', '{ hello: "<div>\n\tWorld\n</div>" };');
  const files = [fileOne, fileTwo];
  const words =
    'code one ' +
    'hello hello world ' +
    'code two ' +
    'hello div world div';
  const wordCount = JSON.stringify({
    code: 2, one: 1,
    hello: 3, world: 2,
    two: 1,
    div: 2
  }, null, 2);

  let outputFileMock;

  beforeEach(async () => {
    outputFileMock = await initOutputFileMock({ files });
  });

  test('writes relativePaths to source-files.txt', async () => {
    expect(await outputFileMock).toBeCalledWith(
        'source-files.txt',
        `${fileOne.relativePath}\n${fileTwo.relativePath}`);
  });

    test('writes words to cloud.txt', async () => {
      expect(await outputFileMock).toBeCalledWith('cloud.txt', words);
    });

  test('writes words counts to cloud.json', async () => {
    expect(await outputFileMock).toBeCalledWith('cloud.json', wordCount);
  });
});

describe('applies rules by', () => {
  const colorsFile = TestFile('colors', 'hello red green blue');
  const numbersFile = TestFile('numbers', 'hello one two three');
  const files = [colorsFile, numbersFile];
  const noHelloWithNumbers = { test: /numbers/, convert: replace(/hello/g, '') };
  const noThree = { test: /numbers/, convert: replace(/three/g, '') };
  const rules = [noHelloWithNumbers, noThree];

  let outputFileMock;

  beforeEach(async () => {
    outputFileMock = await initOutputFileMock({ files, rules });
  });

  test('changing contents of matching files', async () => {
    expect(await outputFileMock).toBeCalledWith(
      'cloud.txt',
      expect.not.stringMatching(/three/));
  });

  test('not altering contents of non-matching files', async () => {
    expect(await outputFileMock).toBeCalledWith(
      'cloud.txt',
      expect.stringMatching(/hello/));
  });
});

async function initOutputFileMock({ files = [], rules = [] }) {
  const _chewDir = jest.fn();
  cloudifyCode({ _chewDir, rules });
  const processAll = _chewDir.mock.calls[0][0].processAll;
  const outputFile = jest.fn();
  await processAll(files, { outputFile });
  return outputFile;
}

function TestFile(name, content) {
  return {
    name,
    relativePath: `dir/${name}.js`,
    read: async () => content
  };
}
